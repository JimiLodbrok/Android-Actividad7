package com.example.mylibrary.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mylibrary.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleMapFragment extends Fragment {
    private TextView nombreJugador;
    private TextView equipoJugador;
    private TextView alturaJugador;
    private ImageView imagenJugador;

    public DetalleMapFragment() {
        // Required empty public constructor
        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detalle_map, container, false);
        nombreJugador = v.findViewById(R.id.nombreJugador);
        equipoJugador = v.findViewById(R.id.equipoJugador);
        alturaJugador = v.findViewById(R.id.alturaJugador);
        imagenJugador = v.findViewById(R.id.imagenJugador);
        return v;
    }

    public TextView getNombreJugador() {
        return nombreJugador;
    }public void setNombreJugador(TextView nombreJugador) {
        this.nombreJugador = nombreJugador;
    }public TextView getEquipoJugador() {
        return equipoJugador;
    }public void setEquipoJugador(TextView equipoJugador) {
        this.equipoJugador = equipoJugador;
    }public TextView getAlturaJugador() {
        return alturaJugador;
    }public void setAlturaJugador(TextView alturaJugador) {
        this.alturaJugador = alturaJugador;
    }public ImageView getImagenJugador() {
        return imagenJugador;
    }public void setImagenJugador(ImageView imagenJugador) {
        this.imagenJugador = imagenJugador;
    }
}
