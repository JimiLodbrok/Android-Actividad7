package com.example.mylibrary.fragments;

import com.facebook.AccessToken;

/**
 * Created by Jaime on 29/01/2018.
 */

public interface FacebookListener {
    void onFBLogin(AccessToken token);
}
