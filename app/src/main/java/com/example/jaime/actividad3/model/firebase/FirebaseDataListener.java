package com.example.jaime.actividad3.model.firebase;

public interface FirebaseDataListener {
    void dataLoaded();
    void addMarkers();
    void removeMarkers();
}
